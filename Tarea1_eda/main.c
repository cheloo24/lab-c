#include <stdio.h>
#include <stdlib.h>
#define MAX_SIZE 200

struct cola{
	int arreglo[MAX_SIZE];
	int primero, ultimo;
};

struct cola *crear(){
	struct cola *queue = malloc(sizeof(struct cola));
	//se usa -1 porque corresponde a una posicion nula dentro del arreglo
	queue->primero = -1;
	queue->ultimo = -1;
	return queue;
}

int esVacia(struct cola *queue){
	return (queue->primero == queue->ultimo);
}

struct cola *enqueue(int x, struct cola *queue){
	if(queue->ultimo-queue->primero == MAX_SIZE){
		printf("La cola est� llena");
	}
	else {
		queue->ultimo++;
		queue->arreglo[queue->ultimo%MAX_SIZE]=x;
	}
	return queue;
}

struct cola *dequeue(struct cola *queue){
	if(esVacia(queue)){
		printf("La cola esta vac�a");
	}
	else{
		queue->primero++;
	}
	return queue;
}

int frente(struct cola *queue){
	if(!esVacia(queue)){
		return queue->arreglo[(queue->primero+1)%MAX_SIZE];
	}
	else{
		return ' ';
	}
}

struct cola *anular(struct cola *queue){
	queue->primero = -1;
	queue->ultimo = -1;
	return queue;
}

void mostrar(struct cola *queue){
    int i;
	for(i=0; i<queue->ultimo; i++){
		printf("%i \n",queue->arreglo[i]);
	}
}

int main(){
	struct cola *A = crear();
	enqueue(2,A);
	enqueue(3,A);
	enqueue(4,A);
	enqueue(5,A);
	enqueue(6,A);
	mostrar(A);

	return 0;
}
