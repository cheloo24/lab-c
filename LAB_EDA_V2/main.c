#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


typedef struct Datos_elecciones{
    char *id;
    char *nombre;
    char *region;
    char *partido;
    int edad;
}Datos;


typedef struct nodo{
    Datos *persona;
    struct nodo *sgte;
}Nodo;

typedef struct lista{
    Nodo *inicio;
    Nodo *fin;
    int tam;
}Lista;

void inicia_lista(Lista *lista){
    lista->inicio = NULL;
    lista->fin = NULL;
    lista->tam = 0;
}

void inserta_en_lista_vacia(Lista *lista, Datos *persona){
    Nodo *nodo_aux;
    if((nodo_aux = (Nodo *)malloc(sizeof(Nodo))) == NULL)
        printf("Error 1");
    else if((nodo_aux->persona = (Datos *)malloc(sizeof(Datos))) == NULL)
        printf("Error 1");
    else{
        nodo_aux->persona->id = (char*)malloc(30*sizeof(char));
        nodo_aux->persona->nombre = (char*)malloc(128*sizeof(char));
        nodo_aux->persona->partido = (char*)malloc(10*sizeof(char));
        nodo_aux->persona->region = (char*)malloc(10*sizeof(char));
        strcpy(nodo_aux->persona->id,persona->id);
        strcpy(nodo_aux->persona->nombre,persona->nombre);
        strcpy(nodo_aux->persona->region,persona->region);
        strcpy(nodo_aux->persona->partido,persona->partido);
        nodo_aux->persona->edad = persona->edad;
        nodo_aux->sgte = NULL;
        lista->inicio = nodo_aux;
        lista->fin = nodo_aux;
        lista->tam++;

    }
}

void inserta_en_inicio_lista(Lista *lista, Datos *persona){
    Nodo *nodo_aux;
    if((nodo_aux = (Nodo *)malloc(sizeof(Nodo))) == NULL)
        printf("Error 1");
    else if((nodo_aux->persona = (Datos *)malloc(sizeof(Datos))) == NULL)
            printf("Error 2");
    else{
        nodo_aux->persona->id = (char*)malloc(30*sizeof(char));
        nodo_aux->persona->nombre = (char*)malloc(128*sizeof(char));
        nodo_aux->persona->partido = (char*)malloc(10*sizeof(char));
        nodo_aux->persona->region = (char*)malloc(10*sizeof(char));
        strcpy(nodo_aux->persona->id,persona->id);
        strcpy(nodo_aux->persona->nombre,persona->nombre);
        strcpy(nodo_aux->persona->region,persona->region);
        strcpy(nodo_aux->persona->partido,persona->partido);
        nodo_aux->persona->edad = persona->edad;
        nodo_aux->sgte = lista->inicio;
        lista->inicio = nodo_aux;
        lista->tam++;

    }
}


int validarFichero(char *fichero){
    FILE *Fichero;
    Fichero = fopen(fichero,"r");
    if(Fichero==NULL)
        return -1;
    else
        return 0;
}

Lista *lectura_archivo(char * fichero){
    FILE *Fichero;
    Fichero = fopen(fichero,"r");
    char *buffer = (char *)malloc(512*sizeof(char));
    int i = 0;
    Datos *persona = (Datos *)malloc(sizeof(Datos));
    Lista *lista = (Lista *)malloc(sizeof(Lista));
    inicia_lista(lista);
    char * aux = (char*)malloc(128*sizeof(char));
    persona->id = (char*)malloc(30*sizeof(char));
    persona->nombre = (char*)malloc(128*sizeof(char));
    persona->partido = (char*)malloc(10*sizeof(char));
    persona->region = (char*)malloc(10*sizeof(char));
    i = 0;
    while(fgets(buffer,512,Fichero)!= NULL){
        //printf("%s\n",buffer);
        if(i==0){
            aux = strtok(buffer,",");
            strcpy(persona->id,aux);
            aux = strtok(NULL, ",");
            strcpy(persona->nombre,aux);
            aux = strtok(NULL, ",");
            strcpy(persona->region,aux);
            aux = strtok(NULL, ",");
            strcpy(persona->partido,aux);
            aux = strtok(NULL, ",");
            persona->edad = atoi(aux);

            inserta_en_lista_vacia(lista,persona);



        }
    else{
            aux = strtok(buffer,",");
            strcpy(persona->id,aux);
            aux = strtok(NULL, ",");
            strcpy(persona->nombre,aux);
            aux = strtok(NULL, ",");
            strcpy(persona->region,aux);
            aux = strtok(NULL, ",");
            strcpy(persona->partido,aux);
            aux = strtok(NULL, ",");
            persona->edad = atoi(aux);
            aux = strtok(NULL, ",");
            inserta_en_inicio_lista(lista,persona);





        }
    i++;

    }
    printf("Lectura realizada con exito...");
    fclose(Fichero);
    return lista;

}

void imprime_lista(Lista *lista){
    Nodo *aux_nodo;
    aux_nodo = lista->inicio;
    int tamano = lista->tam;
    int i;
        for(i = 0;i<tamano;i++){
        printf("%s\n",aux_nodo->persona->id);
        printf("%s\n",aux_nodo->persona->nombre);
        printf("%s\n",aux_nodo->persona->partido);
        printf("%s\n",aux_nodo->persona->region);
        printf("%d\n",aux_nodo->persona->edad);
        aux_nodo = aux_nodo->sgte;
    }
}

void consultar_por_ID(Lista *lista,char* id){
    Nodo *aux_nodo;
    aux_nodo = lista->inicio;
    int tamano = lista->tam;
    int i;
    for(i = 0;i<tamano;i++){
        if(strcmp(aux_nodo->persona->id,id)== 0){
            printf("Su rut es : %s\n",aux_nodo->persona->id);
            printf("Su nombre es : %s\n",aux_nodo->persona->nombre);
            printf("Pertenece al partido : %s\n",aux_nodo->persona->partido);
            printf("Registra en la %s region\n",aux_nodo->persona->region);
            printf("Su edad es : %d\n",aux_nodo->persona->edad);

        }
        else{
            aux_nodo = aux_nodo->sgte;
        }
    }
}

void consultar_por_Region(Lista *lista,char* region){
    Nodo *aux_nodo;
    aux_nodo = lista->inicio;
    int tamano = lista->tam;
    int i;
    for(i = 0;i<tamano;i++){
        if(strcmp(aux_nodo->persona->region, region)== 0){
            printf("Los adherentes de la %s region son: \n",region);
            printf("Su rut es : %s\n",aux_nodo->persona->id);
            printf("Su nombre es : %s\n",aux_nodo->persona->nombre);
            printf("Pertenece al partido : %s\n",aux_nodo->persona->partido);
            printf("Registra en la %s region\n",aux_nodo->persona->region);
            printf("Su edad es : %d\n",aux_nodo->persona->edad);
            aux_nodo = aux_nodo->sgte;
        }
        else
            aux_nodo = aux_nodo->sgte;
    }
}

void consultar_por_Partido(Lista *lista,char *partido){
    Nodo *aux_nodo;
    aux_nodo = lista->inicio;
    int contador = 0;
    int tamano = lista->tam;
    int i;
    for(i = 0;i<tamano;i++){
        if(strcmp(aux_nodo->persona->partido, partido)== 0){
            contador++;
            printf("Los adherentes del partido %s son: \n",partido);
            printf("Su rut es : %s\n",aux_nodo->persona->id);
            printf("Su nombre es : %s\n",aux_nodo->persona->nombre);
            printf("Pertenece al partido : %s\n",aux_nodo->persona->partido);
            printf("Registra en la %s region\n",aux_nodo->persona->region);
            printf("Su edad es : %d\n",aux_nodo->persona->edad);
            aux_nodo = aux_nodo->sgte;
        }
        else
            aux_nodo = aux_nodo->sgte;
    }

}

Lista *consultar_personas_en_varios_partidos(Lista *lista){
    Lista *fraudes;
    Nodo *aux1 = lista->inicio;
    Nodo *aux2 = lista->inicio;
    int i = 0;

    while(aux1 != NULL){
        while(aux2 != NULL){
            if(strcmp(aux1->persona->id, aux2->persona->id)== 0 && strcmp(aux1->persona->partido,aux2->persona->partido)!= 0 && aux1!=aux2){
                fraudes = (Lista *)malloc(sizeof(Lista));
                inicia_lista(fraudes);
                if(i==0){
                    inserta_en_lista_vacia(fraudes,aux1->persona);
                    i++;
                }
                else
                    inserta_en_inicio_lista(fraudes,aux1->persona);
            }
            else{
                aux2 = aux2->sgte;
            }
        }
        aux1 = aux1->sgte;
    }
    return fraudes;
}

void elimina_por_ID(Lista *lista,char *id){
    int i;
    int tamano = lista->tam;
    Nodo *nodo_aux; //Lo uso para manter enlazada la lista
    Nodo *nodo_sup; // Nodo a borrar
    nodo_aux = lista->inicio;
    for(i =1 ;i<tamano;i++){
        if(strcmp(nodo_aux->persona->id, id)== 0){
            nodo_sup = nodo_aux->sgte;
            nodo_aux->sgte = nodo_aux->sgte->sgte;
            if(nodo_aux->sgte == NULL)
                lista->fin = nodo_aux;
            free(nodo_sup->persona);
            free(nodo_sup);
            lista->tam--;
        }
        else
            nodo_aux = nodo_aux->sgte;
    }
    printf("Eliminado con exito...");


}



/*void calcula_estadistica_por_partido(Lista *lista){
    Nodo *aux1 = lista->inicio;
    Nodo *aux2 = lista->inicio;
    //int total = lista->tam;
    int contador_region = 0;
    int porcentaje_region = 0;
    int porcentaje_nacional = 0;
    int contador_nacional = 0;

    while(aux1 != NULL){
        while(aux2 != NULL){
            if(aux1->persona->partido == aux2->persona->partido && aux1->persona->region == aux2->persona->region && aux1!=aux2)
                contador_region++;

            else if(aux1->persona->partido == aux2->persona->partido && aux1->persona->region != aux2->persona->region && aux1!=aux2)
                    contador_nacional++;
            else
                aux2 = aux2->sgte;

        }
        aux1 = aux1->sgte;
    }
}
*/

int main(){
    char *archivo;
    archivo = (char *)malloc(sizeof(char));


         int seleccion,repetir=1;

         while (repetir==1){
            printf("\nArchivo de personal");
            printf("\n1. Agregar un archivo de datos a la lista");
            printf("\n2. Consultar los datos de un adherente por medio de su ID");
            printf("\n3. Consultar los datos de los adherentes de un partido");
            printf("\n4. Consultar los datos de los adherentes por Regi�n");
            printf("\n5. Consultar los adherentes que est�n inscritos en m�s de un partido pol�tico");
            printf("\n6. Eliminar los datos de un adherente usando su ID");
            printf("\n7. Eliminar los adherentes que est�n inscritos en m�s de un partido pol�tico");
            printf("\n8. Salir");

            printf("\nSeleccione una opcion \n");
            seleccion=getche();


            switch(seleccion){
                case '1':

                    printf("\nIngrese la ruta del archivo: \n");
                    scanf("%s",archivo);
                    Lista *lista;
                    if(validarFichero(archivo)== -1){
                        printf("Error, no se encuentra el archivo de texto...");
                        getch();
                    }
                    else{
                        lista = lectura_archivo(archivo);

                    }
                    getch();

                    break;

                case '2':
                    if(validarFichero(archivo)== -1)
                        printf("Error, no se encuentra el archivo de texto...");
                    char * ID = (char *)malloc(20*sizeof(char));
                    printf("\nIngrese rut sin puntos y con gui�n (Ejemplo 12333444-k)\n");
                    scanf("%s",ID);
                    consultar_por_ID(lista,ID);
                    getch();

                    break;

                case '3':
                    if(validarFichero(archivo)== -1)
                        printf("Error, no se encuentra el archivo de texto...");
                    char * partido = (char*)malloc(10*sizeof(char));
                    printf("\nIngrese un partido : \n");
                    scanf("%s",partido);
                    getch();

                    break;

                case '4':
                    if(validarFichero(archivo)== -1)
                        printf("Error, no se encuentra el archivo de texto...");
                    char* region = (char*)malloc(5*sizeof(char));
                    printf("\nIngrese una region : \n");
                    scanf("%s",region);
                    consultar_por_Region(lista,region);
                    getch();

                    break;

                case '5':
                    if(validarFichero(archivo)== -1)
                        printf("Error, no se encuentra el archivo de texto...");
                    imprime_lista(consultar_personas_en_varios_partidos(lista));
                    getch();

                    break;

                case '6':
                    if(validarFichero(archivo)== -1)
                        printf("Error, no se encuentra el archivo de texto...");
                    char * ID1 = (char *)malloc(20*sizeof(char));
                    printf("\nIngrese rut sin puntos y con gui�n (Ejemplo 12333444-k) \n");
                    scanf("%s",ID1);
                    elimina_por_ID(lista,ID1);
                    getch();

                    break;
                case '7':

                    imprime_lista(consultar_personas_en_varios_partidos(lista));
                    printf("Ingrese el rut del adherente que desea eliminar:\n");
                    scanf("%s",ID);
                    elimina_por_ID(lista,ID);
                    break;
                case '8':
                    exit(0);
                    break;


          }//cierra switch
         }//cierra while

return 0;
}

