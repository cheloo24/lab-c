#include <stdlib.h>
#include <stdio.h>

struct nodo{
	int dato;
	int altura;
	struct nodo *izq;
	struct nodo *der;
};
struct nodo *crear(){
	struct nodo *A=malloc(sizeof(struct nodo));
	A->izq=NULL;
	A->der=NULL;
	return A;
}
int maximo(int a, int b){
   if(a>b){
      return a;
   }
   else{
      return b;
   }
}
int Altura(struct nodo * n){
   if(n==NULL){
      return -1;
   }
   else{
      return n->altura;
   }
}
struct nodo * girarDerecha(struct nodo * n){
   struct nodo * aux = malloc(sizeof(struct nodo));
   aux = n->izq;
   n->izq = aux->der;
   aux->der = n;
   n = aux;
   aux = n->der;
   aux->altura = maximo(Altura(aux->izq),Altura(aux->der))+1;
   n->altura = maximo(Altura(n->izq),Altura(n->der))+1;
   return n;
}
struct nodo * girarIzquierda(struct nodo * n){
   struct nodo * aux = malloc(sizeof(struct nodo));
   aux = n->der;
   n->der = aux->izq;
   aux->izq = n;
   n = aux;
   aux = n->izq;
   aux->altura = maximo(Altura(aux->izq),Altura(aux->der))+1;
   n->altura = maximo(Altura(n->izq),Altura(n->der))+1;
   return n;
}
struct nodo * girarIzqDoble(struct nodo * n){
   n->izq = girarIzquierda(n->izq);
   n = girarDerecha(n);
   return n;
}
struct nodo * girarDerDoble(struct nodo * n){
   n->der = girarDerecha(n->der);
   n = girarIzquierda(n);
   return n;
}
struct nodo * adaptarAVL(struct nodo * n, int x){
   if(n!=NULL){
      if (x > n->dato){
         n->der = adaptarAVL(n->der,x);
      }
      else if (x < n->dato){
         n->izq = adaptarAVL(n->izq,x);
      }
      switch (Altura(n->izq)-Altura(n->der)){
         case 2:
            if (Altura(n->izq) > Altura(n->der)){
               n = girarDerecha(n);
            }
            else{
               n = girarDerDoble(n);
            }
            break;
         case -2:
            if (Altura(n->der) > Altura(n->izq)){
               n = girarIzquierda(n);
            }
            else{
               n = girarIzqDoble(n);
            }
            break;
         default:
            n->altura = maximo(Altura(n->izq),Altura(n->der))+1;
      }
   }
   return n;
}
struct nodo *insertar(int x, struct nodo *raiz){

	if(raiz==NULL){
		raiz=crear();
		raiz->dato=x;
	}else{
		if(x>raiz->dato)
			raiz->der=insertar(x,raiz->der);
		else
			raiz->izq=insertar(x,raiz->izq);
	}
	return raiz;
}
struct nodo * agregarNodoArbol2(int x, struct nodo * n){
   n = insertar(x,n);
   n = adaptarAVL(n,x);
   return n;
}
int buscar(int x,struct nodo*raiz){
	if(raiz==NULL){
		return 0;
	}else{
		if(x==raiz->dato){
			return 1;
		}else{
			if(x>raiz->dato)
				return buscar(x,raiz->der);
			else
				return buscar(x,raiz->izq);

		}
	}
}
int busca_menor_mayores(struct nodo *raiz){
	struct nodo *aux=raiz;
	aux=aux->der;
	while(aux->izq!=NULL){
		aux=aux->izq;
	}
	return aux->dato;
}
int esHoja(struct nodo *raiz){
	if(raiz->der==NULL&&raiz->izq==NULL)
		return 1;
	return 0;
}
struct nodo *eliminar(int x, struct nodo *raiz){
	struct nodo *aux;
	if(raiz!=NULL){
		if(x==raiz->dato){
			if(esHoja(raiz)){
				free(raiz);
				raiz=NULL;
			}else{
				if(raiz->izq!=NULL&&raiz->der==NULL){
					aux=raiz;
					raiz=raiz->izq;
					free(aux);
				}else{
					if(raiz->izq==NULL&&raiz->der!=NULL){
						aux=raiz;
						raiz=raiz->der;
						free(aux);
					}else{
						raiz->dato=busca_menor_mayores(raiz);
						raiz->der=eliminar(raiz->dato,raiz->der);
					}
				}
			}
		}else{
			if(x>raiz->dato)
				raiz->der = eliminar(x,raiz->der);
			else
				raiz->izq = eliminar(x,raiz->izq);
		}

	}
	return raiz;
}
void preorden(struct nodo* raiz){
	if(raiz!=NULL){
		printf("%i - ",raiz->dato);
		preorden(raiz->izq);
		preorden(raiz->der);
	}
}
void inorden(struct nodo* raiz){
	if(raiz!=NULL){
		inorden(raiz->izq);
		printf("%i - ",raiz->dato);
		inorden(raiz->der);
	}
}
void postorden(struct nodo* raiz){
	if(raiz!=NULL){
		postorden(raiz->izq);
		postorden(raiz->der);
		printf("%i - ",raiz->dato);

	}
}
int main(){
	struct nodo *raiz=NULL;
	raiz=agregarNodoArbol2(7,raiz);
	raiz=agregarNodoArbol2(5,raiz);
	raiz=agregarNodoArbol2(15,raiz);
	raiz=agregarNodoArbol2(2,raiz);
	raiz=agregarNodoArbol2(10,raiz);
	raiz=agregarNodoArbol2(17,raiz);
	raiz=agregarNodoArbol2(13,raiz);
	raiz=eliminar(7,raiz);
	if(buscar(13,raiz))
		printf("encontrado \n");
	else
		printf("no encontrado \n");
	//printf("%i \n",raiz->dato);
	//printf("%i \n",raiz->izq->dato);
	//printf("%i \n",raiz->der->dato);
	//printf("%i \n",raiz->izq->izq->dato);
	preorden(raiz);
	printf("\n");
	inorden(raiz);
	printf("\n");
	postorden(raiz);
	printf("\n");
	free(raiz);

	return 1;
}


