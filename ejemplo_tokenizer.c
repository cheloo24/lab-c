#include <stdio.h>
#include <stdlib.h>
#include <String.h>

#define SIZE_BUFFER 512

//Programa que obtiene los n�meros almacenados en un archivo de texto, los cuales est�n separados por un espacio
int main(){
    FILE *fp = fopen("entrada.txt", "r");
	char *buffer = (char *) malloc (SIZE_BUFFER * sizeof(char)); //Se reserva memoria para el buufer que leer� los datos del archivo
	int i=0;
	
	fgets(buffer, SIZE_BUFFER, fp);//Se lee una linea desde el archivo. fgets leer� una cantidad de 'SIZE_BUFFER' caracteres, o hasta que encuentre un salto de linea o el final del archivo
	
	fclose(fp);
	
	int largo = strlen(buffer); //Indica el largo de la cadena. Esta funci�n para de contar hasta encontarse con el caracter /0 que indica el final de un String 
    
    //Se calcula la cantidad de numeros en la cadena 'buffer' seg�n la cantidad de espacios en blanco que tiene
    int contador = 1;
    for (i=0; i<largo; i++){
        if (buffer[i] == ' ')    
           contador++;
    }
    
	int *arreglo = (int *) malloc (contador * sizeof(int));
	
	i=0;
	char *aux = (char *) malloc (SIZE_BUFFER/4 * sizeof(char)); //Se reserva memoria para la variable que guardar� las partes a cortar desde la cadena almacenada en buffer
    aux = strtok(buffer, " ");	//Se llama por primera vez a strtok, que va a cortar la cadena cada vez que encuentre un espacio en ella
    while(aux != NULL && i<largo){//strtok destrulle la cadena que esta cortando
		arreglo[i] = atoi(aux);//Se guarda el elemento le�do en el arreglo. Atoi transforma un String en entero
		aux = strtok(NULL, " ");//Si no se coloca el valor NULL, se entra en un bucle infinito
		i++;
    }
    
    for (i=0; i<contador; i++){
        printf("Arreglo[%d]: %d\n", i, arreglo[i]);    
    }
    getch();
    
    return 0;
}
