#include <stdlib.h>
#include <stdio.h>
#define long_max 200

struct lista{
	int elem[long_max];
	int ultimo;
};

struct lista *crear(){
	struct lista *L=malloc(sizeof(struct lista));
	L->ultimo=0;
	return L;
}

struct lista *insertar(int x, int p, struct lista *L){
	int q;

	if(L->ultimo>=long_max){
		printf("Lista llena\n");
	}else{
		if(p<0||p>L->ultimo){
			printf("Posicion no existe");
		}else{
			for(q=L->ultimo;q>p;q--)
				L->elem[q]=L->elem[q-1];
			L->ultimo=L->ultimo+1;
			L->elem[p]=x;
		}
	}
	return L;
}

struct lista *eliminar(int p, struct lista* L){
	int q;

	if(p<0||p>=L->ultimo){
		printf("Posicion no existe");
	}else{
		L->ultimo=L->ultimo-1;
		for(q=p;q<L->ultimo;q++)
			L->elem[q]=L->elem[q+1];
	}
	return L;
}

int localizar(int x,struct lista* L){
	int i=0;
	while(i<L->ultimo){
		if(x==L->elem[i])
			return i;
		i++;
	}
	return -1;
}

int recuperar(int p,struct lista* L){
	if(p<0||p>=L->ultimo){
		printf("Posicion no existe");

	}else{
		return L->elem[p];
	}
	return -1;
}

void mostrar(struct lista* L){
	int i=0;
	while(i<L->ultimo){
		printf("%i-",L->elem[i]);
		i++;
	}
	printf("\b \n");
}

main(){
	struct lista *L=crear();
	L=insertar(100,0,L);
	L=insertar(200,1,L);
	L=insertar(300,0,L);
	mostrar(L);
	L=eliminar(2,L);
	mostrar(L);
	printf("%i\n",localizar(200,L));
	printf("%i\n",recuperar(1,L));
}





