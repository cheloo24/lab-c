#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct lista{
    int contenedor;
    char*dia;
    int peso;
    struct lista * sgte;
};

struct cola{
    struct lista *adelante;
    struct lista *atras;
};

struct cola *crear(){
    struct cola *Q;
    struct lista *nodo;
    Q=malloc(sizeof(struct cola));
    nodo=malloc(sizeof(struct lista));
    Q->adelante = nodo;
    Q->atras = nodo;
    nodo->sgte = NULL;
    return Q;
}
int esVacia(struct cola*Q){
    if(Q->adelante == Q->atras)
        return 1;
    else
        return 0;
}

struct cola * enqueue(int num,char* d,int peso, struct cola *Q){ //AGREGA AL FINAL DE LA COLA
    struct lista *nodo = malloc(sizeof(struct lista));
    nodo->sgte=NULL;
    strcpy(nodo->dia,d);
    nodo->contenedor = num;
    printf("%i",nodo->contenedor);
    nodo->peso = peso;
    Q->atras->sgte=nodo;
    Q->atras = nodo;
    return Q;
}
struct cola *dequeue(struct cola *Q){//elimina de la cabeza
    struct lista *temp;
    if(esVacia(Q)== 1)
        printf("Cola Vacia\n");
    else{
        temp =Q->adelante->sgte;
        Q->adelante->sgte = Q->adelante->sgte->sgte;
        free(temp);
    }
    return Q;
}

void verificaPeso(struct cola *Q){ // Imprime el numero de contenedor, el dia y el peso.
    if(esVacia(Q))
        printf("Cola Vacia\n");
    else{
        while(Q->adelante->sgte !=NULL){
            if(Q->atras->peso>10){
                printf("%i - %s - %i",Q->atras->contenedor,Q->atras->dia,Q->atras->peso);
                dequeue(Q);
                Q->adelante->sgte = Q->adelante->sgte->sgte;
            }
            else
                Q->adelante->sgte = Q->adelante->sgte->sgte;
        }
    }
}

int main()
{

    struct cola * contenedores = malloc(sizeof(struct cola));
    contenedores = crear();

    contenedores = enqueue(01,"Lunes",10,contenedores);
    contenedores = enqueue(02,"Martes",11,contenedores);
    verificaPeso(contenedores);



    return 0;
}
