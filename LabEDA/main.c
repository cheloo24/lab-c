#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef struct datos{
    char *nombre;
    char *id;
    char *region;
    char *partido;
    int edad;
}Datos;

char *crearArreglo(int tam){
    char *aux = (char *)malloc(tam*sizeof(char));
    int i;
    if(aux == NULL)
        return aux;
    else{
        for(i=0;i<tam;i++){
            aux[i] = '\0';
        }
        return aux;
    }

}

Datos *crear_lista_datos(int tam){
    Datos *aux = (Datos *)malloc(tam*sizeof(Datos));
    int i;
    if(aux == NULL)
        return aux;
    for(i=0;i<tam;i++){
        aux[i].id = crearArreglo(10);
        aux[i].nombre = crearArreglo(128);
        aux[i].region = crearArreglo(3);
        aux[i].partido = crearArreglo(100);
        aux[i].edad = 0;
    }
    return aux;
}

int tamanoLinea(char *fichero){
    FILE *Fichero;
    Fichero = fopen(fichero,"r");
    int contador = 0;
    while(fgetc(Fichero)!= '\n'){
        contador++;
    }
    return contador;
}

int cuentaLineas(char *fichero){
    FILE *Fichero;
    Fichero = fopen(fichero,"r");
    int contador = 0;
    char c = getc(Fichero);
    while(!feof(Fichero)){
        if(c == '\n')
            contador++;
            c = getc(Fichero);
    }
    return contador;
}
int validarFichero(char *fichero){
    FILE *Fichero;
    Fichero = fopen(fichero,"r");
    if(Fichero==NULL)
        return 0;
    else
        return 1;
}

Datos *lecturaFichero(char *ficheroDeEntrada){
    FILE *Fichero;

    int i=0;

    char *datos = crearArreglo(300);

    Datos *personas = crearArreglo(cuentaLineas(ficheroDeEntrada));

    Fichero = fopen(ficheroDeEntrada,"r");

    while(fgets(datos, 100, Fichero)!=NULL){
        strcpy(personas[i].id, strtok(datos,","));
        strcpy(personas[i].nombre, strtok(NULL,","));
        strcpy(personas[i].region, strtok(NULL,","));
        strcpy(personas[i].partido, strtok(NULL,","));
        personas[i].edad = atoi(strtok(NULL,","));
        i++;
    }

    fclose(Fichero);

    return personas;
}

int main()
{
    char *nombreFichero = crearArreglo(20);
    nombreFichero = "prueba.txt";

    Datos *personas;

    if(!validarFichero(nombreFichero)){
        printf("Error, no se encuentra el archivo de texto...");
        return 0;
    }

    personas = lecturaFichero(nombreFichero);

    int i = 0;
    for(i = 0; i<cuentaLineas(nombreFichero); i++){
        printf("RUT: %s\n", personas[i].id);
        printf("Nombre completo: %s\n", personas[i].nombre);
        printf("Region: %s\n", personas[i].region);
        printf("Partido: %s\n", personas[i].partido);
        printf("Edad: %i\n", personas[i].edad);
    }


    return 0;
}

