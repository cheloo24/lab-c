#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Datos_producto{

    char* nombre;
    char* categoria;
    char* marca;
    int sku;
    int precio;

}Datos;

typedef struct nodo{
	Datos *producto;
	struct nodo *izq;
	struct nodo *der;
}Nodo;


Nodo *crear_nodo(Datos *p){
	Nodo *raiz;
	if((raiz = (Nodo *)malloc(sizeof(Nodo))) == NULL)
        printf("Error 1");
    else if((raiz->producto = (Datos *)malloc(sizeof(Datos))) == NULL)
        printf("Error 1");
    else{
        raiz->producto->marca=malloc(20*sizeof(char));
        raiz->producto->nombre = malloc(20*sizeof(char));
        raiz->producto->categoria = malloc(20*sizeof(char));
        strcpy(raiz->producto->marca,p->marca);
        strcpy(raiz->producto->nombre,p->nombre);
        strcpy(raiz->producto->categoria,p->categoria);
        raiz->producto->sku = p->sku;
        raiz->producto->precio = p->precio;
        raiz->izq=NULL;
        raiz->der=NULL;

    }
	return raiz;
}

Nodo *insertar(Datos *dato,Nodo *raiz){
	if(raiz!=NULL){
		if(dato->sku>raiz->producto->sku)
			raiz->der=insertar(dato,raiz->der);
		else
			raiz->izq=insertar(dato,raiz->izq);
	}else{
		raiz=crear_nodo(dato);
		printf("\n Procedimiento exitoso!");

	}
	return raiz;
}

void muestra_camino(int num, Nodo *raiz){
   	if(raiz!=NULL){
		while(num!=raiz->producto->sku){
            if(num>raiz->producto->sku){
                printf("%i-",raiz->producto->sku);
                raiz=raiz->der;
                break;
                }
            if(num<raiz->producto->sku){
				printf("%i-",raiz->producto->sku);
                raiz=raiz->izq;
                break;
				}
            }
            printf("%i\n",num);
	}
	else{
		printf(" ");
	}
}

void inorden(Nodo* raiz){
	if(raiz!=NULL){
		inorden(raiz->izq);
		printf("%d -",raiz->producto->sku);
		inorden(raiz->der);
	}
}

void Descendente(Nodo* raiz){

	if(raiz!=NULL){

		Descendente(raiz->der);
		printf("%i -",raiz->producto->sku);
        Descendente(raiz->izq);
	}
}

int cuenta_elementos(Nodo * raiz){
    if(raiz==NULL){
        return 0;
    }
    return (1 + cuenta_elementos(raiz->izq) + cuenta_elementos(raiz->der) );
}
int Profundidad(Nodo *r) {
    int izquierdo=0, derecho = 0;
    if(r==NULL)
        return 0;
    if(r->izq != NULL)
        izquierdo = Profundidad(r->izq);
    if(r->der != NULL)
        derecho = Profundidad(r->der);
    if( izquierdo> derecho)
        return izquierdo+1;
    else
        return derecho+1;
}
Datos *busca_menor_mayores(Nodo *der){
	Nodo *aux=der;
	while(aux->izq!=NULL)
		aux=aux->izq;
	return aux->producto;
}
Nodo *eliminar(int num,Nodo *raiz){
	Nodo *temp;
	if(raiz!=NULL){
		if(num>raiz->producto->sku){
			return eliminar(num,raiz->der);
		}else{
			if(num<raiz->producto->sku){
				return eliminar(num,raiz->izq);
			}else{
				if((raiz->izq==NULL)&&(raiz->der==NULL)){
				    temp = raiz;
					free(raiz);
					raiz=NULL;
					return raiz;
				}
				if((raiz->izq!=NULL)&&(raiz->der==NULL)){
					temp=raiz;
					raiz=raiz->izq;
					free(temp);
					return raiz;
				}
				if((raiz->izq==NULL)&&(raiz->der!=NULL)){
					temp=raiz;
					raiz=raiz->der;
					free(temp);
					return raiz;
				}
				if((raiz->izq!=NULL)&&(raiz->der!=NULL)){
					raiz->producto = busca_menor_mayores(raiz->der);
					raiz->der=eliminar(raiz->producto->sku,raiz->der);
					return raiz;
				}
			}
		}
	}
	return raiz;
}
void muestra_menor(Nodo *raiz){
    if(raiz->izq!=NULL){
       muestra_menor(raiz->izq);
    }
    else{
    printf("\nEl menor elemento es el: %i\n",raiz->producto->sku);
    }
}

void muestra_mayor(Nodo *raiz){
    if(raiz->der!=NULL){
        muestra_mayor(raiz->der);

    }
    else  {
        printf("\nEl mayor elemento es el: %i\n",raiz->producto->sku);
    }
}

Nodo *espejar_arbol(Nodo *raiz){
    Nodo *temp;

    if(raiz!=NULL)
    {
          temp = raiz->izq;
          raiz->izq = espejar_arbol(raiz->der);
          raiz->der = espejar_arbol(temp);
    }
    printf("\nArbol espejado exitosamente!!!");
    return raiz;
}

int verifica_mismos_elementos(Nodo *arbol1, Nodo *arbol2){
int aux=0;
if(arbol1!=NULL && arbol2!=NULL){
    if(arbol1->producto->sku==arbol2->producto->sku){
        aux=1;
       }
       else{
            return 0;

       }
    verifica_mismos_elementos(arbol1->der,arbol2->der);
    verifica_mismos_elementos(arbol1->izq,arbol2->izq);
}
return aux;
}

int verifica_son_iguales(Nodo *arbol1, Nodo *arbol2){
    if(verifica_mismos_elementos(arbol1,arbol2)==1){
        if(cuenta_elementos(arbol1)==cuenta_elementos(arbol2)){
            if(arbol1->producto->sku==arbol2->producto->sku){
                return 1;
            }
        }
    }
    return 0;

}

int validarFichero(char *fichero){
    FILE *Fichero;
    Fichero = fopen(fichero,"r");
    if(Fichero==NULL)
        return -1;
    else
        return 0;
}


Nodo *lectura_archivo(char * fichero){
    FILE *Fichero;
    Fichero = fopen(fichero,"r");
    char *buffer = (char *)malloc(512*sizeof(char));

    Nodo * raiz;
    Datos* dato = (Datos*)malloc(sizeof(Datos));
    dato->categoria=(char*)malloc(20*sizeof(char));
	dato->nombre=(char*)malloc(30*sizeof(char));
	dato->marca=(char*)malloc(30*sizeof(char));
    char * aux = (char*)malloc(128*sizeof(char));
    while(fgets(buffer,512,Fichero)!= NULL){
        aux = strtok(buffer,";");
        dato->sku = atoi(aux);
        aux = strtok(NULL, ";");
        strcpy(dato->nombre,aux);
        aux = strtok(NULL, ";");
        strcpy(dato->marca,aux);
        aux = strtok(NULL, ";");
        dato->precio = atoi(aux);
        aux = strtok(NULL, ";");
        strcpy(dato->categoria,aux);

        raiz = insertar(dato,raiz);
        printf("Cargando.....\n");
    }
    printf("Lectura realizada con exito...\n");
    fclose(Fichero);
    return raiz;

}

void menu(){
    char i;
    int sku;

    char *archivo=(char *) malloc(20*sizeof(char));

    printf("\nBienvenido al Laboratorio 2 de EDA !! \n");
    printf("\nIngrese la ruta del archivo en el que se encuentren los datos: \n");
    scanf("%s",archivo);
    Nodo *arbol1 = malloc(sizeof(Nodo));
    arbol1 = lectura_archivo(archivo);
    Descendente(arbol1);



    while(1){

        printf("\n\t-------------------------MENU-------------------------\n\n\n");
        printf("\n(b) Insertar un dato");
        printf("\n(c) Eliminar un dato por su clave de busqueda");
        printf("\n(d) Lista de datos ordenados ascendentemente por su clave de busqueda");
        printf("\n(e) Lista de datos ordenados desscendentemente por su clave de busqueda");
        printf("\n(f) Contar elementos");
        printf("\n(g) Calcular la profundidad del arbol de datos");
        printf("\n(h) Ver menor elemento");
        printf("\n(i) Ver mayor elemento");
        printf("\n(j) Mostrar camino raiz-hoja de un producto");
        printf("\n(k) Espejar el arbol");
        printf("\n(l) Verificar si 2 arboles son iguales");
        printf("\n(m) Verificar si 2 arboles tienen los mismos elementos");
        printf("\n(n) Salir\n\n");

        i = getche();
        switch(i){

            case 'b':
                    printf("\nIngrese el nombre del producto :\n");
                    Datos *producto = malloc(sizeof(Datos));
                    producto->nombre = (char *)malloc(30*sizeof(char));
                    producto->marca = (char *)malloc(30*sizeof(char));
                    producto->categoria = (char *)malloc(30*sizeof(char));


                    scanf("%s",producto->nombre);
                    printf("\nIngrese la marca del producto :\n");
                    scanf("%s",producto->marca);
                    printf("\nIngrese la categoria del producto :\n");
                    scanf("%s",producto->categoria);
                    printf("\nIngrese el sku o numero de busqueda del producto :\n");
                    scanf("%i",&producto->sku);
                    printf("\nIngrese el precio :\n");
                    scanf("%i",&producto->precio);
                    arbol1 = insertar(producto,arbol1);
                    getch();

                    break;
            case 'c':
                    printf("\nIngrese el sku del producto que desea eliminar:\n");
                    scanf("%i",&sku);
                    eliminar(sku,arbol1);
                    getch();
                    break;
            case 'd':
                    printf("\n\n");
                    inorden(arbol1);
                    getch();
                   break;
            case 'e':
                    printf("\n\n");
                    Descendente(arbol1);
                    getch();
                    break;
            case 'f':
                    printf("\n\nEl numero de elementos es : %i",cuenta_elementos(arbol1));
                    getch();

                    break;
            case 'g':
                    printf("\n\nLa profundidad del arbol es : %i",Profundidad(arbol1));
                    getch();

                   break;
            case 'h':
                    printf("\n\n");
                    muestra_menor(arbol1);
                    getch();
                   break;
            case 'i':
                    printf("\n\n");
                    muestra_mayor(arbol1);
                    getch();
                   break;
            case 'j':
                    printf("\nIngrese el sku del producto del cual desea ver su camino:\n");
                    scanf("%i",&sku);
                    muestra_camino(sku,arbol1);
                    getch();
                    break;
            case 'k':
                    arbol1 = espejar_arbol(arbol1);
                    getch();
                   break;
            case 'l':
                    printf("\nIngrese la ruta del archivo en el que se encuentren los datos a comparar: \n");
                    Nodo *arbol2 = malloc(sizeof(Nodo));
                    char *archivo2=(char *) malloc(20*sizeof (char));
                    scanf("%s",archivo2);
                    arbol2 = lectura_archivo(archivo2);
                    if(verifica_son_iguales(arbol1,arbol2)== 1)
                        printf("\n Los arboles son iguales!!");
                    else
                        printf("\n Los arboles son distintos!!");
                    getch();
                   break;
            case 'm':
                    printf("\nIngrese la ruta del archivo en el que se encuentren los datos a comparar: \n");
                    scanf("%s",archivo2);
                    arbol2 = lectura_archivo(archivo2);
                    if(verifica_mismos_elementos(arbol1,arbol2)== 1)
                        printf("\n Los arboles tienen los mismos elementos!!");
                    else
                        printf("\n Los arboles tienen distintos elementos!!");
                    getch();
                   break;
            case 'n':exit(0);

            default: break;
        }
    }
}
int main(){
    //Nodo* arbol = malloc(sizeof(Nodo));
    //arbol = lectura_archivo("bd.txt");
    menu();

	return 0;
}



