#include <stdio.h>
#include <stdlib.h>

#define LONG_MAX 200
struct cola{
	int inicio;
	int fin;
	char elem[LONG_MAX];
};

struct cola *crearCola(){
	struct cola *Q=malloc(sizeof(struct cola));
	
	Q->inicio=-1;
	Q->fin=-1;
	return Q;
}

int es_vaciaCola(struct cola *Q){
	return(Q->inicio==Q->fin);
}

struct cola *enqueue(char x, struct cola *Q){
	if(Q->fin-Q->inicio==LONG_MAX)
		printf("cola llena \n");
	else{
		Q->fin++;
		Q->elem[Q->fin%LONG_MAX]=x;
	}
	return Q;
}

struct cola *popCola(struct cola *Q){
	if(es_vaciaCola(Q))
		printf("cola Vacia \n");
	else{
		Q->inicio++;
	}
	return Q;
}

char frente(struct cola *Q){
	if(!es_vaciaCola(Q))
		return Q->elem[(Q->inicio+1)%LONG_MAX];
	else
		return ' ';
}

struct pila{
	int tope;
	char elem[LONG_MAX];
};

struct pila *crearPila(){
	struct pila *S=malloc(sizeof(struct pila));
	S->tope=0;
	return S;
}

int es_vaciaPila(struct pila *S){
	return(S->tope==0);
}

struct pila *push(char x, struct pila *S){
	if(S->tope>=LONG_MAX-1)
		printf("Pila llena \n");
	else{
		S->tope++;
		S->elem[S->tope]=x;
	}
	return S;
}

struct pila *popPila(struct pila *S){
	if(es_vaciaPila(S))
		printf("Pila Vacia \n");
	else{
		S->tope--;
	}
	return S;
}

char tope(struct pila *S){
	return S->elem[S->tope];
}


int esPalindromo(char palabra[]){
	int i;
	struct cola *palabra_cola = crearCola();
	struct pila *palabra_pila = crearPila();
	int longitud = strlen(palabra);
	for(i =0 ; i < longitud ;i++){
		palabra_cola = enqueue(palabra[i] ,palabra_cola);
		palabra_pila = push(palabra[i], palabra_pila);
	}
	
	
	for( i = 0; i<longitud ;i++){
		if(tope(palabra_pila) != frente(palabra_cola)){
			printf(palabra);
			printf(" no es palindromo\n");
			return 0;
		}
		else{
			palabra_pila = popPila(palabra_pila);
			palabra_cola = enqueue(frente(palabra_cola),palabra_cola); 
			palabra_cola = popCola(palabra_cola);
		}
		
	}
	printf(palabra);
	printf(" es palindromo !! \n");
	return 0;
}

int main(){
	
	esPalindromo("sos");
	esPalindromo("hola");
	esPalindromo(" ");
	esPalindromo(".");
		
}
