#include <stdio.h>
#include <stdlib.h>

#define LONG_MAX 200
struct cola{
	int inicio;
	int fin;
	char elem[LONG_MAX];
};

struct cola *crear(){
	struct cola *Q=malloc(sizeof(struct cola));
	
	Q->inicio=-1;
	Q->fin=-1;
	return Q;
}

int es_vacia(struct cola *Q){
	return(Q->inicio==Q->fin);
}

struct cola *enqueue(char x, struct cola *Q){
	if(Q->fin-Q->inicio==LONG_MAX)
		printf("cola llena \n");
	else{
		Q->fin++;
		Q->elem[Q->fin%LONG_MAX]=x;
	}
	return Q;
}

struct cola *pop(struct cola *Q){
	if(es_vacia(Q))
		printf("cola Vacia \n");
	else{
		Q->inicio++;
	}
	return Q;
}



char frente(struct cola *Q){
	if(!es_vacia(Q))
		return Q->elem[(Q->inicio+1)%LONG_MAX];
	else
		return ' ';
}

struct cola *anular(struct cola *Q){
	Q->inicio=-1;
	Q->fin=-1;
	return Q;
}

int main(){
}
