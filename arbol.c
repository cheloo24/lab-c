#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct nodo{
	char *dato;
	struct nodo *izq;
	struct nodo *der;
};

struct nodo *crear_nodo(char *dato){
	struct nodo *raiz= malloc(sizeof(struct nodo));
	raiz->dato=malloc(sizeof(char)*10);
	strcpy(raiz->dato,dato);
	raiz->izq=NULL;
	raiz->der=NULL;
	return raiz;
}

struct nodo *insertar(char *dato,struct nodo *raiz){
	if(raiz!=NULL){
		if(strcmp(dato,raiz->dato)>0)
			raiz->der=insertar(dato,raiz->der);
		else
			raiz->izq=insertar(dato,raiz->izq);
	}else{
		raiz=crear_nodo(dato);
	}
	return raiz;
}

int buscar(char *dato, struct nodo *raiz){
	if(raiz!=NULL){
		if(strcmp(dato,raiz->dato)>0){
			return buscar(dato,raiz->der);
		}else{
			if(strcmp(dato,raiz->dato)<0){
				return buscar(dato,raiz->izq);
			}else{
				return 1;
			}
		}
	}else{
		return 0;
	}
}

char *busca_menor_mayores(struct nodo *der){
	struct nodo *aux=der;
	while(aux->izq!=NULL)
		aux=aux->izq;
	return aux->dato;
}

struct nodo *eliminar(char *dato,struct nodo *raiz){
	struct nodo *temp;
	if(raiz!=NULL){
//Agregar busqueda del elemento
		if(strcmp(dato,raiz->dato)>0){
			return eliminar(dato,raiz->der);
		}else{
			if(strcmp(dato,raiz->dato)<0){
				return eliminar(dato,raiz->izq);
			}else{
				if((raiz->izq==NULL)&&(raiz->der==NULL)){
					free(raiz);
					raiz=NULL;
					return raiz;
				}
				if((raiz->izq!=NULL)&&(raiz->der==NULL)){
					temp=raiz;
					raiz=raiz->izq;
					free(temp);
					return raiz;
				}
				if((raiz->izq==NULL)&&(raiz->der!=NULL)){
					temp=raiz;
					raiz=raiz->der;
					free(temp);
					return raiz;
				}
				if((raiz->izq!=NULL)&&(raiz->der!=NULL)){
					strcpy(raiz->dato,busca_menor_mayores(raiz->der));
					raiz->der=eliminar(raiz->dato,raiz->der);
					return raiz;
				}
			}
		}		
	}
	return raiz;
}
void preorden(struct nodo* raiz){
	if(raiz!=NULL){
		printf("%s -",raiz->dato);
		preorden(raiz->izq);
		preorden(raiz->der);
	}
}
void inorden(struct nodo* raiz){
	if(raiz!=NULL){
		inorden(raiz->izq);
		printf("%s -",raiz->dato);
		inorden(raiz->der);
	}
}
void postorden(struct nodo* raiz){
	if(raiz!=NULL){
		postorden(raiz->izq);
		postorden(raiz->der);
		printf("%s -",raiz->dato);
	}
}
int main(){
	struct nodo *arbol1=NULL;
	arbol1 = insertar("X",arbol1);
	arbol1 = eliminar("X",arbol1);
	arbol1 = insertar("D",arbol1);
	arbol1 = insertar("E",arbol1);
	arbol1 = insertar("B",arbol1);
	arbol1 = insertar("S",arbol1);
	arbol1 = insertar("C",arbol1);
	arbol1 = eliminar("D",arbol1);

	preorden(arbol1);
	printf("\n");
	inorden(arbol1);
	printf("\n");
	postorden(arbol1);
	printf("\n");
	//printf("La raiz contiene el dato: %s\n",arbol1->dato);
	//printf("El hijo izquierdo contiene el dato: %s\n",arbol1->izq->dato);

	//if(buscar("D",arbol1))
	//	printf("El hijo derecho contiene el dato: %s\n",arbol1->der->dato);
	//else
	//	printf("El dato no existe\n");
	return 0;
}





