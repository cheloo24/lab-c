#include <stdio.h>
#include <stdlib.h>

#define LONG_MAX 200
struct pila{
	int tope;
	char elem[LONG_MAX];
};

struct pila *crear(){
	struct pila *S=malloc(sizeof(struct pila));
	S->tope=0;
	return S;
}

int es_vacia(struct pila *S){
	return(S->tope==0);
}

struct pila *push(char x, struct pila *S){
	if(S->tope>=LONG_MAX-1)
		printf("Pila llena \n");
	else{
		S->tope++;
		S->elem[S->tope]=x;
	}
	return S;
}

struct pila *pop(struct pila *S){
	if(es_vacia(S))
		printf("Pila Vacia \n");
	else{
		S->tope--;
	}
	return S;
}



char tope(struct pila *S){
	return S->elem[S->tope];
}
struct pila *anular(struct pila *S){
	S->tope=0;
	return S;
}

int main(){
}
