#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define B 10

typedef struct celda
 {
   char *nombre;
   struct celda *next;
 } tcelda, *pcelda;

static pcelda hashtab[B];

void makenull(void)
{ int i;
for ( i = 0; i < B; i++) hashtab[i] = NULL;
}

char *strsave(char *s)
{
  char *p;
  if (( p = malloc(strlen(s) + 1)) != NULL)
  strcpy(p,s);
  return (p);
}


int h(char *s)
{ int hval;
  for (hval =0; *s!='\0';) hval+= *s++;
  return (hval % B);
}

pcelda buscar(char *s)
{
   pcelda cp;
   for (cp = hashtab[h(s)]; cp!= NULL; cp = cp->next)
   if (strcmp(s, cp->nombre ) == 0)  return (cp);
   return (NULL);
}

pcelda inserte(char *s)
{ pcelda cp;
  int hval;
  if (( cp = buscar(s)) == NULL) {
      cp = (pcelda ) malloc(sizeof (tcelda ));
      if (cp == NULL) return (NULL);
      if (( cp-> nombre = strsave(s)) == NULL ) return (NULL);
      hval = h(cp -> nombre);
      cp -> next = hashtab[hval];
      hashtab[hval] = cp;
  }
  return (cp);
}

int main(void)
{
  /*makenull();
  if(inserte("hola") == NULL) printf("no lo insert�\n");
  if(inserte("holb") == NULL) printf("no lo insert�\n");
    if( buscar("hola") == NULL) printf("no lo encontr�\n"); else printf("encontrado\n");
  descarte("hola");
  if( buscar("hola") == NULL) printf("no lo encontr�\n"); else printf("encontrado\n");*/
  return 1;
}
