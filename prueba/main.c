#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


typedef struct Datos_elecciones{
    char *id;       //Rut
    char *nombre;   //Nombre completo
    char *region;     //Numero de la region que corresponde. Obs: RM = XIII
    char *partido;  //Partido que pertenece
    int edad;       //Edad de la persona
}Datos;

typedef struct nodo{
    Datos persona;
    struct nodo *sgte;
}Nodo;

struct nodo *crear(){// Crea un nodo.
    struct nodo *L = (struct nodo *)malloc(sizeof(struct nodo));
    L->sgte=NULL;
    return L;
}

void insertar(int edad, char *id, char *nombre, char *partido, char *region, struct nodo *p){ // "Constructor", inserta en un nodo los datos de una persona.
    struct nodo * temp;
    temp = p->sgte;
    p->sgte= (struct nodo *)malloc(sizeof(struct nodo));
    p->sgte->persona.edad=edad;
    p->sgte->persona.nombre=nombre;
    p->sgte->persona.partido=partido;
    p->sgte->persona.region=region;
    p->sgte->persona.id=id;
    p->sgte->sgte=temp;
}
void eliminar(char *id,struct nodo *L){  //Elimina mediante ID

    //Localiza primero el elemento, se lo salta, luego lo elimina.

    struct nodo *p;
    while (L->sgte!=NULL){
        if (L->sgte->persona.id==id){
            p=L->sgte;
            L->sgte = L->sgte->sgte; //Se salta el elemento localizado.
            free(p);
        }
        else{
            L=L->sgte;
        }
    }
}


void LocalizarId(char *id, struct nodo *L){ //Localiza una persona por ID e imprime sus datos
    struct nodo *p=L;
    while (p->sgte!=NULL){
        if (p->sgte->persona.id==id){
            printf("%s\b %i\b %s\b %s\n",p->sgte->persona.nombre,p->sgte->persona.edad,p->sgte->persona.partido,p->sgte->persona.region);
            break;
        }

        else
            p=p->sgte;
    }
}

int LocalizarPartido(char *partido, struct nodo *L){ //Localiza las personas de un partido, las imprime x pantalla y retorna la cantidad de personas.
    struct nodo *p=L;
    int contador = 0;
    while (p->sgte!=NULL){
        if (p->sgte->persona.partido==partido){
            printf("%s\b %s\b %i\b %s\n",p->sgte->persona.id,p->sgte->persona.nombre,p->sgte->persona.edad,p->sgte->persona.region);
            contador++;
            p=p->sgte;
        }
        else
            p=p->sgte;
    }
    return contador;
}

struct nodo *Fraudes(struct nodo *L){
    /* Revisa coincidencias de rut y partido
       si tienen igual rut y distinto partido(hay fraude), elimina los 2 nodos de la lista (oficial)
       e inserta los datos de ese ID en la lista de Fraudes.
    */
    struct nodo *lista_fraudes = crear();
    struct nodo *aux1 = L;
    struct nodo *aux2 = L;

    while(aux1->sgte!= NULL){
        while(aux2->sgte!= NULL){
            if(aux1->sgte->persona.id == aux2->sgte->persona.id && aux1->sgte->persona.partido != aux2->sgte->persona.partido && aux2->sgte != L->sgte){
                insertar(aux2->sgte->persona.edad, aux2->sgte->persona.id, aux2->sgte->persona.nombre, aux2->sgte->persona.partido, aux2->sgte->persona.region, lista_fraudes);
                eliminar(aux2->sgte->persona.id, L);
                }
            else
                aux2=aux2->sgte;


        aux1=aux1->sgte;
       }
    }
    return lista_fraudes;

}

int cuentaCaracteres(char *fichero){
    FILE *Fichero;
    Fichero = fopen(fichero, "r");

    int contador = 0;

    while (fgetc(Fichero)!='\n'){
            contador++;
    }

    return contador;
}

int cuentaLineas(char *fichero) {
    FILE *Fichero;
    int cont = 0;

    Fichero = fopen(fichero, "r");

    char c = getc(Fichero);
    while (!feof(Fichero)) {
        if (c == '\n')
            cont++;
        c = getc(Fichero);
    }

    fclose(Fichero);

    return cont;
}

int validarFichero(char *fichero){
    FILE *Fichero;
    Fichero = fopen(fichero,"r");
    if(Fichero==NULL)
        return 0;
    else
        return 1;
}


struct nodo*lecturaFichero(char *entradaFichero){
    FILE *Fichero;

    char datos[100];
    struct nodo *personas = crear();


    Fichero = fopen(entradaFichero,"r");

    printf("llegue aqui  1\n");


    while(fgets(datos,100, Fichero)!=NULL){

        printf("llegue aqui  02 %s\n",datos);

        printf("%s\n",personas->persona.id);
        strcpy(personas->persona.id, strtok(datos,","));

        printf("%s\n",personas->persona.id);

        printf("llegue aqui  3");


        strcpy(personas->persona.nombre, strtok(NULL,","));


        printf("llegue aqui  4");
        //printf("%s\n",personas->persona.region);

        strcpy(personas->persona.region, strtok(NULL,","));
        printf("%s\n",personas->persona.region);

        strcpy(personas->persona.partido, strtok(NULL,","));

        printf("llegue aqui  5");

        personas->persona.edad = atoi(strtok(NULL,","));

        printf("llegue aqui  6");

        personas = personas->sgte;

        printf("llegue aqui  7");


    }
    if(feof(Fichero)){
        fclose(Fichero);
    }

    else{
        printf("Error en la lectura del archivo...");
    }


    return personas;
}

int LocalizarRegion(char *region, struct nodo *L){ // Localiza por region,imnprime los datos y retorna la cantidad
    struct nodo *p=L;
    int contador = 0;
    while (p->sgte!=NULL){
        if (p->sgte->persona.region==region){
            printf("%s\b %s\b %i\b %s\n",p->sgte->persona.id,p->sgte->persona.nombre,p->sgte->persona.edad,p->sgte->persona.partido);
            contador++;
            p=p->sgte;
        }

        else
            p=p->sgte;
    }
    return contador;
}



int main()
{


    char *nombreFichero = (char *)malloc(100*sizeof(char));
    nombreFichero = "prueba.txt";
    struct nodo *personas;

    if(!validarFichero(nombreFichero)){
        printf("Error, no se encuentra el archivo de texto...");
        return 0;
    }
    else{
        personas = (lecturaFichero(nombreFichero));
        //LocalizarId("18724210-k",personas);
    }



return 0;
}
