#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


typedef struct Datos_elecciones{
    char *id;       //Rut
    char *nombre;   //Nombre completo
    char *region;     //Numero de la region que corresponde. Obs: RM = XIII
    char *partido;  //Partido que pertenece
    int edad;       //Edad de la persona
}Datos;


typedef struct nodo{
    Datos *persona;
    struct nodo *sgte;
}Nodo;

typedef struct lista{
    Nodo *inicio;
    Nodo *fin;
    int tam;
}Lista;

void iniciaLista(Lista *lista){
    lista->inicio = NULL;
    lista->fin = NULL;
    tam = 0;
}


