#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* COMPILADO EN WINDOWS 7 CODEBLOCKS 10.05
    AUTOR: MARCELO ACEVEDO */


typedef struct Productos{
int sku;
char* nombre;
char* marca;
int precio;
char* categoria;
}Datos;


typedef struct nodo{
	Datos* producto;
	struct nodo *izq;
	struct nodo *der;
}nodo;


nodo *crear_nodo(Datos *produc){
	nodo *raiz = malloc(sizeof(nodo));
	raiz->producto->categoria=malloc(sizeof(char)*512);
	raiz->producto->nombre=malloc(sizeof(char)*512);
	raiz->producto->marca=malloc(sizeof(char)*512);
	raiz->producto->sku = produc->sku;
	raiz->producto->precio = produc->precio;

	strcpy(raiz->producto->nombre,produc->nombre);
	printf("lo hice bien 1");

	//strcpy(raiz->producto->marca,produc->marca);
	printf(produc->marca);
	printf("lo hice bien 2");
	strcpy(raiz->producto->categoria,produc->categoria);
	printf("lo hice bien 3");
	raiz->izq=NULL;
	printf("lo hice bien 4");
	raiz->der=NULL;
	printf("lo hice bien 5");
	return raiz;
}

int cuenta_elementos(nodo * raiz){
    if(raiz==NULL){
        return 0;
    }
    return (1 + cuenta_elementos(raiz->izq) + cuenta_elementos(raiz->der) );
}

nodo *insertar(Datos *p,nodo *raiz){ //Insertar en orden
	if(raiz!=NULL){
		if(p->sku > raiz->producto->sku)
			raiz->der=insertar(p,raiz->der);
		else
			raiz->izq=insertar(p,raiz->izq);
	}else{
	    printf("llegue aqui");
		raiz=crear_nodo(p);
		printf("Se ha insertado con exito su dato");
	}
	return raiz;
}

nodo* buscar(int num, nodo *raiz){
	if(raiz!=NULL){
		if(num>raiz->producto->sku){
			return buscar(num,raiz->der);
		}else{
			if(num<raiz->producto->sku){
				return buscar(num,raiz->izq);
			}else{
				return raiz;
			}
		}
	}else{
		return raiz;
	}
}

Datos *busca_menor_mayores(nodo *der){
	nodo *aux=der;
	while(aux->izq!=NULL)
		aux=aux->izq;
	return aux->producto;
}
int Profundidad(nodo *r) {
    int izquierdo=0, derecho = 0;
    if(r==NULL)
        return 0;
    if(r->izq != NULL)
        izquierdo = Profundidad(r->izq);
    if(r->der != NULL)
        derecho = Profundidad(r->der);
    if( izquierdo> derecho)
        return izquierdo+1;
    else
        return derecho+1;
}

void muestraCamino(nodo *t,int *arrg,int indice){
    int i;
    if(t==NULL){
        return;
    }
    arrg[indice]=t->producto->sku;
    if(t->izq==NULL && t->der==NULL){
        printf("\nEl camino es:\n");
        for(i=0;i<=indice;i++){
            printf(" %d",arrg[i]);
        }
    }else{
        muestraCamino(t->izq,arrg,(indice+1));
        muestraCamino(t->der,arrg,(indice+1));
    }
}

nodo *eliminar(int num,nodo *raiz){
	nodo *temp;
	if(raiz!=NULL){
		if(num>raiz->producto->sku){
			return eliminar(num,raiz->der);
		}else{
			if(num<raiz->producto->sku){
				return eliminar(num,raiz->izq);
			}else{
				if((raiz->izq==NULL)&&(raiz->der==NULL)){
				    temp = raiz;
					free(raiz);
					raiz=NULL;
					return raiz;
				}
				if((raiz->izq!=NULL)&&(raiz->der==NULL)){
					temp=raiz;
					raiz=raiz->izq;
					free(temp);
					return raiz;
				}
				if((raiz->izq==NULL)&&(raiz->der!=NULL)){
					temp=raiz;
					raiz=raiz->der;
					free(temp);
					return raiz;
				}
				if((raiz->izq!=NULL)&&(raiz->der!=NULL)){
					raiz->producto = busca_menor_mayores(raiz->der);
					raiz->der=eliminar(raiz->producto->sku,raiz->der);
					return raiz;
				}
			}
		}
	}
	return raiz;
}
void preorden(nodo* raiz){
	if(raiz!=NULL){
		printf("%i -",raiz->producto->sku);
		preorden(raiz->izq);
		preorden(raiz->der);
	}
}
void inorden(nodo* raiz){
	if(raiz!=NULL){
		inorden(raiz->izq);
		printf("%i -",raiz->producto->sku);
		inorden(raiz->der);
	}
}
void postorden(nodo* raiz){
	if(raiz!=NULL){
		postorden(raiz->izq);
		postorden(raiz->der);
		printf("%i -",raiz->producto->sku);
	}
}

int validarFichero(char *fichero){
    FILE *Fichero;
    Fichero = fopen(fichero,"r");
    if(Fichero==NULL)
        return -1;
    else
        return 0;
}

nodo *lectura_archivo(char * fichero){
    FILE *Fichero;
    Fichero = fopen(fichero,"r");
    char *buffer = (char *)malloc(512*sizeof(char));

    Datos* product = (Datos*)malloc(sizeof(Datos));
    nodo * raiz = malloc(sizeof(nodo));
    product->categoria=(char*)malloc(sizeof(char)*128);
	product->nombre=(char*)malloc(sizeof(char)*128);
	product->marca=(char*)malloc(sizeof(char)*128);
    char * aux = (char*)malloc(128*sizeof(char));
    while(fgets(buffer,512,Fichero)!= NULL){
        //printf("%s\n",buffer);
        aux = strtok(buffer,";");
        product->sku = atoi(aux);
        aux = strtok(NULL, ";");
        strcpy(product->nombre,aux);
        aux = strtok(NULL, ";");
        strcpy(product->marca,aux);
        aux = strtok(NULL, ";");
        product->precio = atoi(aux);
        aux = strtok(NULL, ";");
        strcpy(product->categoria,aux);
        raiz = insertar(product,raiz);
    }
    printf("Lectura realizada con exito...");
    fclose(Fichero);
    return raiz;

}

int main(){

	char *archivo;
    archivo = (char *)malloc(sizeof(char));
    nodo* arbol1;
    Datos *dato = malloc(sizeof(Datos));

         int seleccion,repetir=1;

         while (repetir==1){
            printf("\n  Menu");
            printf("\na. Ingrese la direccion en la que se encuentran los datos:");
            printf("\nb. Insertar un dato");
            printf("\nc. Eliminar un dato por su clave de busqueda");
            printf("\nd. Lista de datos ordenados ascendentemente por su clave de busqueda");
            printf("\ne. Lista de datos ordenados desscendentemente por su clave de busqueda");
            printf("\nf. Contar elementos");
            printf("\ng. Calcular la profundidad del arbol de datos");
            printf("\nh. Ver menor elemento");
            printf("\ni. Ver mayor elemento");
            printf("\nj. Mostrar caminos raiz-hoja de todos las hojas");
            printf("\nk. Espejar el arbol");
            printf("\nl. Verificar si 2 arboles son iguales");
            printf("\nm. Verificar si 2 arboles tienen los mismos elementos");
            printf("\nn. Salir");

            printf("\nSeleccione una opcion \n");
            seleccion = getche();


            switch(seleccion){
                case 'a':

                    printf("\nIngrese la ruta del archivo: \n");
                    scanf("%s",archivo);

                    if(validarFichero(archivo)== -1){
                        printf("Error, no se encuentra el archivo de texto...");
                        getch();
                    }
                    else{
                        arbol1 = lectura_archivo(archivo);

                    }
                    getch();

                    break;

                case 'b':
                    dato->nombre = (char *)malloc(128*sizeof(char));
                    dato->marca = (char *)malloc(128*sizeof(char));
                    dato->categoria = (char *)malloc(128*sizeof(char));
                    printf("\nIngrese el nombre del producto :\n");
                    scanf("%s",dato->nombre);
                    printf("\nIngrese la marca del producto :\n");
                    scanf("%s",dato->marca);
                    printf("\nIngrese la categoria del producto :\n");
                    scanf("%s",dato->categoria);
                    printf("\nIngrese el sku o numero de busqueda del producto :\n");
                    scanf("%i",&dato->sku);
                    printf("\nIngrese el precio :\n");
                    scanf("%i",&dato->precio);
                    arbol1 = insertar(dato,arbol1);
                    getch();

                    break;
                case 'c':

                    printf("\nIngrese el c�digo de b�squeda o sku del elemento que desea eliminar :\n");
                    int numero;
                    scanf("%i",&numero);
                    arbol1 = eliminar(numero,arbol1);
                    getch();

                    break;

                case 'd':
                    inorden(arbol1);
                    getch();

                    break;

                case 'e':

                    getch();

                    break;

                case 'f':


                    break;

                case 'g':

                    getch();

                    break;

                case 'h':

                    getch();

                    break;

                case 'i':

                    getch();

                    break;

                case 'j':

                    getch();

                    break;

                case 'k':

                    getch();

                    break;

                case 'l':

                    getch();

                    break;

                case 'm':

                    getch();

                    break;

                case 'n':
                    exit(0);
                    break;
            }
         }



	return 0;
}



