#include <stdio.h>
#include <stdlib.h>


struct conjunto{
	int conjunto[30];
};

struct conjunto *insertar(int x, struct conjunto *A){
	if(x < 30 && x > 0){
		A->conjunto[x] = 1;
		return A;
	}
	else
		return A;
}

struct conjunto *suprime(int x, struct conjunto *A){
	if(x < 30 && x > 0){
		A->conjunto[x] = 0;
		return A;
	}
	else
		return A;
}

int miembro(int x, struct conjunto *A){
	if(x < 30 && x > 0){
		return A->conjunto[x];
	}
	else
		return ' ';
}

struct conjunto *Union(struct conjunto *A, struct conjunto *B){
	struct conjunto *C = malloc(sizeof(struct conjunto));
	int i;

	for(i=0; i<30; i++){
		if((A->conjunto[i] == 1) || (B->conjunto[i] == 1)){
			C->conjunto[i] = 1;
		}
	}
	return C;
}

struct conjunto *interseccion(struct conjunto *A, struct conjunto *B){
	struct conjunto *C = malloc(sizeof(struct conjunto));
	int i;

	for(i=0; i<30; i++){
		if((A->conjunto[i] == 1) && (B->conjunto[i] == 1)){
			C->conjunto[i] = 1;
		}
	}
	return C;
}

int main(){
	struct conjunto *A = malloc(sizeof(struct conjunto));
	struct conjunto *B = malloc(sizeof(struct conjunto));
	int i;
	for(i=0; i<30; i++){
		A->conjunto[i] = 0;
		B->conjunto[i] = 0;
	}
	insertar(1,A);
	insertar(28,A);
	insertar(20,A);
	insertar(2,B);
	insertar(15,B);
	insertar(23,B);

	struct conjunto *C = interseccion(A,B);

	for(i=0; i<30; i++){
		printf("%i: \n", C->conjunto[i]);
	}

	return 0;
}
