#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LONG_MAX 11
#define M LONG_MAX

#define N_MAX 25
char *tabla[LONG_MAX];

void crear(){
	int i;
	for(i=0;i<M;i++){
		tabla[i]=malloc(sizeof(char)*N_MAX);
		strcpy(tabla[i],"");
		//tabla[i]=NULL;
	}
}
int funcion_hash(char *frase){
	int n = strlen(frase);
	int i,suma=0;
	for(i=0;i<n;i++){
		//printf("%i\n",frase[i]);
		suma=suma+(i+1)*frase[i];
	}
	printf("%i\n",suma);
	if(n>0)
		return suma%M;
	else
		return -1;
}

void insertar(char *frase){
	int hash=funcion_hash(frase);
	printf("%i\n",hash);
	int i;
	if(hash!=-1){
		if(strcmp(tabla[hash],"")==0)
			strcpy(tabla[hash],frase);
		else{ //hay colisión
			for(i=hash+1;i<hash+M;i++){
			//rehashing lineal en incremento de 1
				if(strcmp(tabla[i%M],"")==0){
					strcpy(tabla[i%M],frase);
					break;
				}
			}
		}
	}else{
		printf("La frase de entrada es inválida");
	}
}
void mostrar(){
	int i;
	for(i=0;i<M;i++){
		if(strcmp(tabla[i],"")!=0){
			printf("%i %s\n",i,tabla[i]);
		}
	}
}

int main(){
	//char *a;
	//scanf("%s",a);
	//printf("frase: %s \n",a);
	//printf("posición: %i \n",funcion_hash(a));
	crear();
	insertar("hola");
	insertar("chao");
	insertar("olah");
	insertar("ocah");
	mostrar();
	return 0;
}
