#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LONG 15

struct nodo{
	char *dato;
	struct nodo *anterior;
	struct nodo *siguiente;
};

struct nodo *crear(char *dato){
	struct nodo *lista= malloc(sizeof(struct nodo));
	lista->dato=malloc(sizeof(char)* 10);
	strcpy(lista->dato,dato);
	lista->anterior=NULL;
	lista->siguiente=NULL;
	return lista;
}

struct nodo *insertar(char *dato,struct nodo *lista){
	if(lista!=NULL){
		if(strcmp(dato,lista->dato)>0)
			lista->siguiente=insertar(dato,lista->siguiente);
		else
			lista->anterior=insertar(dato,lista->anterior);
	}else{
		lista=crear(dato);
	}
	return lista;
}

int buscar(char *dato, struct nodo *lista){
	if(lista!=NULL){
		if(strcmp(dato,lista->dato)>0){
			return buscar(dato,lista->siguiente);
		}else{
			if(strcmp(dato,lista->dato)<0){
				return buscar(dato,lista->anterior);
			}else{
				return 1;
			}
		}
	}else{
		return 0;
	}
}

int main(){
	struct nodo *list= malloc(sizeof(struct nodo));
	list = insertar("X",list);
	list = insertar("F",list);
	list = insertar("D",list);
	list = insertar("A",list);
	list = insertar("P",list);

	printf("\n", list->dato);
	printf("\n", list->siguiente->dato);

	return 0;
}
